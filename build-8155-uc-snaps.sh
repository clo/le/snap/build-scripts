#Copyright (c) 2021 The Linux Foundation. All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are
#met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
#WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
#ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
#BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
#BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
#OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
#IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#!/bin/bash

# Locating directories
cd ../../
WORKSPACE=$(pwd)
SNAP_YAML_DIR=$(pwd)/snap
PREBUILT_TAR_DIR=$WORKSPACE/prebuilts
OUT_DIR=$WORKSPACE/out
EXCLUDE_SNAP_LIST="kernel-snap sa8155p-gadget kernel-tests"

# Compile and clean command
COMPILE_COMMAND="snapcraft --destructive-mode"
CLEAN_COMMAND="snapcraft clean --destructive-mode"

#Kernel Header version
KERNEL_HEADERS="linux-libc-dev=4.14.0-1010.10"

# Setting Colour code variables
RED='\033[0;31m'
GREEN='\033[0;32m'
NO_COLOR='\033[0m'

#Setup kernel header from apt store to root of project
setup_environment () {
        if [ ! -d "$WORKSPACE/kernel-headers" ]
        then
		printf "\nDownloading kernel headers $KERNEL_HEADERS from APT store  \n"
		cd $WORKSPACE
		sudo add-apt-repository ppa:ubuntu-cervinia/build -y
		apt-get download $KERNEL_HEADERS
		dpkg -x linux-libc-dev_4.14.0-1010.10_arm64.deb kernel-headers
	else
		printf "\n${GREEN} Kernel headers are already available at $WORKSPACE ${NO_COLOR}\n"
	fi
}

already_compiled_check () {
	SNAP_NAME="$1"
	if [ ! -z $(find $SNAP_DIR -maxdepth 1 -name *.snap) ]
	then
		return 1
	else
		return 0
	fi
}

create_snap () {
	SNAP_NAME="$1"
	SNAP_DIR=$(find $SNAP_YAML_DIR -maxdepth 2 -name $SNAP_NAME -print -quit)
        if [ -d "$SNAP_DIR" ]
        then
		if already_compiled_check "$SNAP_NAME"
		then
	                if [ -e "$SNAP_DIR/snap/snapcraft.yaml" ]
	                then
	                        cd $SNAP_DIR
				$SNAP_ENV
				$COMPILE_COMMAND
				# Copying snap file Snap project to out dir
				if [ ! -e *.snap ]
				then
					printf "\n${RED} Snap compilation failed for : $SNAP_NAME ${NO_COLOR}\n"
					printf "\n${RED} To reproduce run $SNAP_ENV $COMPILE_COMMAND inside  $SNAP_DIR ${NO_COLOR}\n"
					exit
				fi
				if [ -e *.snap ] && [ ! -e *.tar ] && [ ! -e *.deb ]
		                then
		                        cp -rf *.snap $OUT_DIR/
		                fi
	                else
	                        printf "\n${RED} snap/snapcraft.yaml is missing: $SNAP_DIR ${NO_COLOR}\n"
	                fi
                fi
        fi
}


# Compiling snap from all yaml directory
build_8155_userspace_snaps () {
	mkdir -p out prebuilts
	setup_environment

	create_snap "qti-android-system-core"
	create_snap "qti-adsprpc-prop-noship"
	SNAP_ENV="export DSP_LIB_PATH=$WORKSPACE/src/qcomm-fw"
	create_snap "qti-adsprpc"
	COMPILE_COMMAND="snapcraft --destructive-mode"
	create_snap "qti-securemsm-noship"
	create_snap "qti-securemsm-ship"
	create_snap "qti-fastcv-prop-noship"
	create_snap "qti-fastcv"
	create_snap "gpsd-debian-package"
	create_snap "qti-gnss-noship"
	create_snap "qti-gnss"

}

#Compile/create kernel and gadget snap
kernel_and_gadget_snap () {
	mkdir -p out prebuilts
	SNAP_ENV="export KERNEL_DEBUG_ARTIFACTS=${WORKSPACE}/out/ SNAPDRAGON_KERNEL_SNAP_FIRMWARE=$WORKSPACE/src/qcomm-fw/NON_HLOS/"
	COMPILE_COMMAND="snapcraft --target-arch=arm64 --destructive-mode --enable-experimental-target-arch"
	create_snap "kernel-snap"

	SNAP_ENV="export SA8155P_GADGET_SNAP_SECTOOLS=$WORKSPACE/src/vendor/qcom/proprietary/sectools/  SA8155P_GADGET_SNAP_BOOT_ASSETS=$WORKSPACE/src/qcomm-fw/non_hlos_fw/"
	COMPILE_COMMAND="snapcraft --target-arch=arm64 --destructive-mode --enable-experimental-target-arch"
	create_snap "sa8155p-gadget"
}


# Cleaning snap from all yaml directory
clean () {
	for YAML_DIR in $SNAP_YAML_DIR/yaml*
	do
		for SNAP_DIR in $YAML_DIR/*
		do
			[ -d "$SNAP_DIR" ] || continue
			[ -e "$SNAP_DIR/snap/snapcraft.yaml" ] || continue
			cd $SNAP_DIR
			$CLEAN_COMMAND
			rm -rf *.snap
		done
	done

	rm -rf $PREBUILT_TAR_DIR/*
	rm -rf $OUT_DIR/*
}

case $1 in
        -build-8155-userspace-snaps)
		build_8155_userspace_snaps
        ;;
        -kernel-and-gadget-snap)
		kernel_and_gadget_snap
        ;;
        -cleanall)
		clean
        ;;
        *)
		printf "\n${RED} $0: invalid option ${NO_COLOR}\n\n"
		printf "Supported option: \n"
		printf "\t${GREEN} -build-8155-userspace-snaps ${NO_COLOR}\n"
		printf "\t${GREEN} -kernel-and-gadget-snap ${NO_COLOR}\n"
		printf "\t${GREEN} -cleanall ${NO_COLOR}\n\n"
esac
